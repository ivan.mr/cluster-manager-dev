#!/bin/bash

CLUSTER_NAME = $1
gcloud container clusters create "${CLUSTER_NAME}" \
	--project "airy-deployment-273811" \
	--num-nodes "3" \
    --machine-type n1-standard-1 \
	--zone "us-central1-a" \
	--disk-type "pd-standard" \
	--disk-size "15GB" \
	--enable-autorepair \
    --no-enable-cloud-monitoring \
    --no-enable-cloud-logging
